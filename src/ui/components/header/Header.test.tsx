import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from './Header';

test('render the correct msg', () => {
  render(<Header />);
  const msg = screen.getByText(/Header placeholder/i);
  expect(msg).toBeInTheDocument();
});

test('snapshot', () => {
  const header = render(<Header />);
  expect(header).toMatchSnapshot()
});